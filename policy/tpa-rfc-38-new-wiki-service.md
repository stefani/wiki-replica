---
title: TPA-RFC-38: Setting Up a Wiki Service
---

[[_TOC_]]

Summary: This RFC aims to identify problems with our current gitlab wikis, and the best solution for those issues.

# Background

Currently, our projects that require a wiki use gitlab wikis. Gitlab wikis are rendered with [gollum](https://github.com/gollum/gollum), and editing is controlled by gitlab's permission system.

The problem: gitlab's permission system only allows repo maintainers to edit wiki pages, meaning that users (anonymous or signed in) don't have the permissions required to actually edit the the wiki pages. This has lead the TPA team to create a seperate tpo/tpa/wiki repo so that people without edit permission can at least propose edits for TPA maintainers to accept.

# Proposal

The easiest solution to gitlab's permission issues is to use a wiki service seperately from gitlab. This wiki service can be one that we host, or a service hosted for us by another organization.

## Goals

* Identify requirements for a wiki service
* Find a wiki service that fits these requirements
* Install a new TPA-managed server to host the wiki, or find an organization to host it for us

### Must have

* Users can edit wiki pages without being given extra permissions ahead of time
* Wiki must have a search function
* Users should be able to read and edit pages over a hidden service
* High-availablity for TPA documentation: If the wiki ever goes down, TPA should not be locked out of the docs on how to recover the wiki
* Clear transition plan from gitlab to this new wiki
    * Must be able to use current markup as-is, or be able to convert to a new syntax (with pandoc/sed script/etc.)
* Folder structure: current GitLab wikis have a page/subpage structure (e.g. TPA's `howto/` has all the howto, `service/` has all the service documentation, etc) which need to be implemented as well, this includes having "breadcrumbs" to walk back up the hierarchy, or (ideally) automatic listing of sub-pages
* Single dynamic site, if not static (e.g. we have a single MediaWiki or Dokuwiki, not one MediaWiki per team), rationale is static sites can be archived and not maintained in the long term, while dynamic applications need constant monitoring and maintenance to function properly, so we need to reduce the maintenance burden

### Nice to have

* Different groups under TPO (i.e. TPA, anti-censorship, comms) have their own namespaces in the wiki
    * example: `/tpo/tpa/wiki_page_1`, `/tpo/core/tor/wiki_page_2` or Mediawiki's namespace systems where each team could have their own namespace (e.g. TPA: Anti-censorship: Community: etc)
    * search must apply across namespaces
* integration with anonticket
* integration with existing systems (gitlab, ldap, etc) as an identity provider
* Support offline reading, and batching edits until a connection is restored (i.e. with git)

### Non-Goals

 * Localization  (most important for user-facing, and support.tpo has l10n)
 * confidential content: we put private content in Nextcloud (eg. TPI folder)
 * software-specific documentation: e.g. stem.tpo, arti, ctor documentation (those use their own build systems like a static site generator), we might still want to recommend a single program for documentation (e.g. settle on mkdocs or hugo or lektor)

# Examples or Personas

Examples:

 * bob: a non-technical person who wants to fix some typos and add some resources to a wiki page
     * with current wiki: bob needs to make a gitlab account, and be given developer permissions to the wiki repo (unlikely). alternatively, bob can open a ticket with the proposed changes, and hope a developer gets around to making them. if the wiki has a wiki-replica repo (i.e. like <https://gitlab.torproject.org/tpo/tpa/wiki-replica/>) then bob could also `git clone` the wiki, make the changes, and then create a PR. bob is unlikely to want to go through such a hassle, and will probably just not contribute
     * with a new wiki system fulfilling the "must-have" goals: bob only needs to make a wiki account before being able to edit a wiki page
 * alice: a developer who helps maintain a TPO repository
     * with current wiki: alice can edit any wiki they have permissions for. however if alice wants to edit a wiki they don't have permission for, they will need to go through the same ticket/PR hassle as bob
     * with new wiki: alice will need to make a wiki account in addition to their gitlab account, but will be able to edit any page afterward
 * anonymous cypherpunk:  a person who wants to contribute to a wiki anonymously
     * with current wiki: the cypherpunk will need to follow the same procedure as bob
     * with new wiki: with only the must-have features, cypherpunks can only contribute pseudonymously. if the new wiki supports anonymous contributions, cypherpunks will have no barrier to contribution.
 * xXx_1337_spamlord_xXx: a non-contributor who likes to make spam edits for fun
     * spamlord will also need to follow the same procedure as bob. this makes spamlord unlikely to try to spam much, and any attempts to spam are easily stopped.
     * with new wiki: with only must-have features, spamlord will have the same barriers, and will most likely not spam much. if anonymous contributions are supported, spamlord will have a much easier time spamming, and the wiki team will need to find a solution to stop spamlord.

# Potential Candidates

* [MediaWiki][]: PHP/Mysql wiki platform, supports markdown via extension, used by Wikipedia
* [MkDocs][]: python-based static-site generator, markdown, built-in dev server
* [Hugo][]: popular go-based static site generator, documentation-specific themes exist such as [GeekDocs](https://geekdocs.de/)
* [ikiwiki][]: a git-based wiki with a CGI web interface

[MediaWiki]: https://www.mediawiki.org/wiki/MediaWiki
[MkDocs]: https://www.mkdocs.org/
[Hugo]: https://gohugo.io/
[ikiwiki]: https://ikiwiki.info/

## mediawiki

### Advantages

Polished web-based editor (VisualEditor). 

Supports sub-pages but not in the Main namespace by default. We could use namespaces for teams and subpages as needed in each namespace?

Possible support for markdown with this extension: https://www.mediawiki.org/wiki/Extension:WikiMarkdown status unknown

"Templating", eg. for adding informative banners to pages or sections

Supports private pages (per-user or per-group permissions).

Basic built-in search and supports advanced search plugins (ElasticSearch, SphinxSearch).

packaged in debian

Downsides:

 * limited support for our normal database server, PostgreSQL: https://www.mediawiki.org/wiki/Manual:PostgreSQL key quotes:
   * second-class support, and you may likely run into some bugs
   * Most of the common maintenance scripts work with PostgreSQL; however, some of the more obscure ones might have problems.
   * While support for PostgreSQL is maintained by volunteers, most core functionality is working.
   * migrating from MySQL to PostgreSQL is possible the reverse is harder
   * they are considering removing the plugin from core, see https://phabricator.wikimedia.org/T315396
 * full-text search requires Elasticsearch which is ~non-free software
   * one alternative is SphinxSearch which is considered unmaintained but works in practice (lavamind has maintained/deployed it until recently)
 * no support for offline workflow (there is a git remote, but it's not well maintained and does not work for authenticated wikis)

## mkdocs

internationalization status unclear, possibly a plugin, untested

used by onion docs, could be useful as a software-specific documentation project

major limitation is web-based editing, which require either a GitLab merge request workflow or a custom app.

## hugo

used for research.tpo, the developer portal.

same limitation as mkdocs for web-based editing

## ikiwiki

Not really active upstream anymore, build speed not great, web interface is plain CGI (slow, editing uses a global lock).

# Status

This proposal is currently in the `draft` state. It's published as a draft so users can add goals and must-have features before the RFC is proposed

# References

[Discussion ticket](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40909)
