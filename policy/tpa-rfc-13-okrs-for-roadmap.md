---
title: TPA-RFC-13: Use OKRs for the 2022 roadmap
---

[[_TOC_]]

Summary: switch to OKRs and GitLab milestones to organise the 2022 TPA
roadmap. Avoid a 2022 user survey. Delegate the OKR design to the team
lead.

# Background

For the [2021 roadmap][], we have established a roadmap made of "Must
have", "Need to have", and "Non-goals", along with a quarterly
breakdown. Part of the roadmap was also based on a user [survey][].

[survey]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/roadmap/2021#survey-results
[2021 roadmap]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/roadmap/2021

Recently, TPI started a process of setting OKRs for each
team. Recently, the TPA team lead was asked to provide OKRs for the
team and is working alongside other team leads to learn how to
establish those, in peer-review meetings happening weekly. The TPA
OKRs need to be presented at the October 20th, 2021 all hands.

## Concerns with the roadmap process

The 2021 roadmap is *big*. Even looking at the top-level checklist
items, there are 7 "Must have" and "11 need to have". Those are a lot
of bullet points, and it is hard to wrap your head around. 

The document is 6000 words long (although that includes the survey
results analysis).

The survey takes a long time to create, takes time for users to fill
up, and takes time to analyse.

## Concerns with the survey

The survey is also *big*. It takes a long time to create, fill up, and
even more to process the results. It was a big undertaking the last
time.

# Proposal

## Adopt the OKR process for 2022-Q1 and 2022-Q2

For 2022, we want to try something different. Instead of the long
"to-do list of death", we try to follow the "Objectives and
Key-results" process ([OKR][]) which basically establishes three to
five broad objectives and, under each one, 3 key results.

[OKR]: https://en.wikipedia.org/wiki/OKR

Part of the idea of using OKRs is that there are less of them: 3 to 5
fits well in [working memory][].

Key results also provide clear, easy to review items to see if the
objectives has been filled. We should expect 60 to 70% of the key
results to be completed by the end of the timeline.

[working memory]: https://en.wikipedia.org/wiki/The_Magical_Number_Seven%2C_Plus_or_Minus_Two

## Skip the survey for 2022

We also skip the survey process ([issue 40307](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40307)) this year. We hope this will save some
time for other productive work. We can always do another survey later
in 2022.

## Delegate the OKR design to the team lead

Because the OKRs need to be presented at the all hands on October
20th, the team lead (anarcat) will make the call of the final list
that will be presented there. The OKRs have already been presented to
the team and most concerns have been addressed, but ultimately the
team lead will decide what the final OKRs will look like.

## Timeline

 * 2021-10-07: OKRs discussed within TPA
 * 2021-10-12: OKRs peer review, phase 2
 * 2021-10-14: this proposal adopted, unless objections
 * 2021-10-19: OKRs peer review, phase 3
 * 2021-10-20: OKRs presented at the all hands
 * 2021-Q4: still organised around the [2021 Q4 roadmap][]
 * 2022-Q1, 2022-Q2: scope of the OKRs
 * mid-2022: OKR reviews, second round of 2022 OKRs

[2021 Q4 roadmap]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/roadmap/2021#q4

# Deadline

Considering the tight deadline of the OKR process, this proposal will
be considered adopted within one week, by October 14th 2021.

# Status

This proposal is currently in the `obsolete` state.

# References

See those introductions to OKRs and how they work:

 * <https://rework.withgoogle.com/guides/set-goals-with-okrs/steps/introduction/>
 * <https://rework.withgoogle.com/guides/set-goals-with-okrs/steps/set-objectives-and-develop-key-results/>
 * [issue 40439](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40439): make the OKRs
 * [issue 40307](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40307): 2022 user survey
