Debian 12 [bookworm](https://wiki.debian.org/DebianBookworm) [entered freeze in January 19th 2023](https://lists.debian.org/debian-devel-announce/2023/01/msg00004.html). TPA
is in the process of studying the procedure and hopes to start
immediately after the bullseye upgrade is completed. We have a hard
deadline of [one year after the stable release](https://www.debian.org/security/faq#lifespan), which gives us a
few years to complete this process. Typically, however, we try to
upgrade during the freeze to report (and contribute to) issues we find
during the upgrade, as those are easier to fix during the freeze than
after. In that sense, the deadline is more like the third quarter of
2023.

It is an aggressive timeline, which will like be missed. It is tracked
in the GitLab issue tracker under the [% Debian 12 bookworm
upgrade](https://gitlab.torproject.org/groups/tpo/tpa/-/milestones/6) milestone. Upgrades will be staged in batches, see
[TPA-RFC-20](policy/tpa-rfc-20-bullseye-upgrades) for details on how that was performed in bullseye.

As soon as when the bullseye upgrade is completed, we hope to phase
out the bullseye installers so that new machines are setup with
bullseye.

This page aims at documenting the upgrade procedure, known problems
and upgrade progress of the fleet.

[[_TOC_]]

# Procedure

This procedure is designed to be applied, in batch, on multiple
servers. Do NOT follow this procedure unless you are familiar with the
command line and the Debian upgrade process. It has been crafted by
and for experienced system administrators that have dozens if not
hundreds of servers to upgrade.

In particular, it runs almost completely unattended: configuration
changes are not prompted during the upgrade, and just not applied at
all, which *will* break services in many cases. We use a
[clean-conflicts](https://gitlab.com/anarcat/koumbit-scripts/-/blob/master/vps/clean_conflicts) script to do this all in one shot to shorten the
upgrade process (without it, configuration file changes stop the
upgrade at more or less random times). Then those changes get applied
after a reboot. And yes, that's even more dangerous.

IMPORTANT: if you are doing this procedure over SSH (I had the
privilege of having a console), you may want to [upgrade SSH first](https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information.en.html#ssh-not-available)
as it has a longer downtime period, especially if you are on a flaky
connection.

See the "conflicts resolution" section below for how to handle
`clean_conflicts` output.

 1. Preparation:

        : reset to the default locale
        export LC_ALL=C.UTF-8 &&
        : put server in maintenance &&
        touch /etc/nologin &&
        : install some dependencies
        sudo apt install ttyrec screen debconf-utils deborphan apt-forktracer &&
        : create ttyrec file with adequate permissions &&
        sudo touch /var/log/upgrade-bookworm.ttyrec &&
        sudo chmod 600 /var/log/upgrade-bookworm.ttyrec &&
        sudo ttyrec -a -e screen /var/log/upgrade-bookworm.ttyrec

 2. Backups and checks:

        ( 
          umask 0077 &&
          tar cfz /var/backups/pre-bookworm-backup.tgz /etc /var/lib/dpkg /var/lib/apt/extended_states /var/cache/debconf $( [ -e /var/lib/aptitude/pkgstates ] && echo /var/lib/aptitude/pkgstates ) &&
          dpkg --get-selections "*" > /var/backups/dpkg-selections-pre-bookworm.txt &&
          debconf-get-selections > /var/backups/debconf-selections-pre-bookworm.txt
        ) &&
        ( puppet agent --test || true )&&
        apt-mark showhold &&
        dpkg --audit &&
        : look for dkms packages and make sure they are relevant, if not, purge. &&
        ( dpkg -l '*dkms' || true ) &&
        : look for leftover config files &&
        /usr/local/sbin/clean_conflicts &&
        : make sure backups are up to date in Nagios &&
        printf "End of Step 2\a\n"

 3. Enable module loading (for ferm) and test reboots:

        systemctl disable modules_disabled.timer &&
        puppet agent --disable "running major upgrade" &&
        shutdown -r +1 "bullseye upgrade step 3: rebooting with module loading enabled"

        export LC_ALL=C.UTF-8 &&
        sudo ttyrec -a -e screen /var/log/upgrade-bullseye.ttyrec

 4. Perform any pending upgrade and clear out old pins:

        apt update && apt -y upgrade &&
        : Check for pinned, on hold, packages, and possibly disable &&
        rm -f /etc/apt/preferences /etc/apt/preferences.d/* &&
        rm -f /etc/apt/sources.list.d/backports.debian.org.list &&
        rm -f /etc/apt/sources.list.d/backports.list &&
        rm -f /etc/apt/sources.list.d/bookworm.list &&
        rm -f /etc/apt/sources.list.d/buster-backports.list &&
        rm -f /etc/apt/sources.list.d/experimental.list &&
        rm -f /etc/apt/sources.list.d/incoming.list &&
        rm -f /etc/apt/sources.list.d/proposed-updates.list &&
        rm -f /etc/apt/sources.list.d/sid.list &&
        rm -f /etc/apt/sources.list.d/testing.list &&
        : purge removed packages &&
        apt purge $(dpkg -l | awk '/^rc/ { print $2 }') &&
        apt autoremove -y --purge &&
        : possibly clean up old kernels &&
        dpkg -l 'linux-image-*' &&
        : look for packages from backports, other suites or archives &&
        : if possible, switch to official packages by disabling third-party repositories &&
        dsa-check-packages | tr -d , &&
        printf "End of Step 4\a\n"

 5. Check free space (see [this guide to free up space][]), disable
    auto-upgrades, and download packages:

        systemctl stop apt-daily.timer &&
        sed -i 's#bullseye-security#bookworm-security#' $(ls /etc/apt/sources.list /etc/apt/sources.list.d/*) &&
        sed -i 's/bullseye/bookworm/g' $(ls /etc/apt/sources.list /etc/apt/sources.list.d/*) &&
        apt update &&
        apt -y -d full-upgrade &&
        apt -y -d upgrade &&
        apt -y -d dist-upgrade &&
        df -h &&
        printf "End of Step 5\a\n"

 6. Actual upgrade run:

        : put server in maintenance &&
        sudo touch /etc/nologin &&
        env DEBIAN_FRONTEND=noninteractive APT_LISTCHANGES_FRONTEND=none APT_LISTBUGS_FRONTEND=none UCF_FORCE_CONFFOLD=y \
            apt full-upgrade -y -o Dpkg::Options::='--force-confdef' -o Dpkg::Options::='--force-confold' &&
        printf "End of Step 6\a\n"

 7. Post-upgrade procedures:

        apt-get update --allow-releaseinfo-change &&
        puppet agent --enable &&
        (puppet agent -t --noop || puppet agent -t --noop || puppet agent -t --noop ) &&
        printf "Press enter to continue, Ctrl-C to abort." &&
        read -r _ &&
        (puppet agent -t || true) &&
        (puppet agent -t || true) &&
        (puppet agent -t || true) &&
        rm -f /etc/apt/apt.conf.d/50unattended-upgrades.dpkg-dist /etc/bacula/bacula-fd.conf.ucf-dist /etc/ca-certificates.conf.dpkg-old /etc/cron.daily/bsdmainutils.dpkg-remove /etc/default/prometheus-apache-exporter.dpkg-dist /etc/default/prometheus-node-exporter.dpkg-dist /etc/ldap/ldap.conf.dpkg-dist /etc/logrotate.d/apache2.dpkg-dist /etc/nagios/nrpe.cfg.dpkg-dist /etc/ssh/ssh_config.dpkg-dist /etc/ssh/sshd_config.ucf-dist /etc/sudoers.dpkg-dist /etc/syslog-ng/syslog-ng.conf.dpkg-dist /etc/unbound/unbound.conf.dpkg-dist &&
        printf "\a" &&
        /usr/local/sbin/clean_conflicts &&
        systemctl start apt-daily.timer &&
        echo 'workaround for Debian bug #989720' &&
        sed -i 's/^allow-ovs/auto/' /etc/network/interfaces &&
        printf "End of Step 7\a\n" &&
        shutdown -r +1 "bullseye upgrade step 7: removing old kernel image"

 8. Post-upgrade checks:

        export LC_ALL=C.UTF-8 &&
        sudo ttyrec -a -e screen /var/log/upgrade-bookworm.ttyrec

        apt-mark manual bind9-dnsutils
        apt purge gcc-9-base gcc-10-base
        apt purge $(dpkg -l | awk '/^rc/ { print $2 }') # purge removed packages
        apt autoremove -y --purge
        apt purge $(deborphan --guess-dummy)
        while deborphan -n | grep -q . ; do apt purge $(deborphan -n); done
        apt autoremove -y --purge
        apt clean
        # review and purge older kernel if the new one boots properly
        dpkg -l 'linux-image*'
        # review obsolete and odd packages
        dsa-check-packages | tr -d ,
        printf "End of Step 8\a\n"
        shutdown -r +1 "bullseye upgrade step 8: testing reboots one final time"

[this guide to free up space]: http://www.debian.org/releases/buster/amd64/release-notes/ch-upgrading.en.html#sufficient-space

## Conflicts resolution

When the `clean_conflicts` script gets run, it asks you to check each
configuration file that was modified locally but that the Debian
package upgrade wants to overwrite. You need to make a decision on
each file. This section aims to provide guidance on how to handle
those prompts.

Those config files should be manually checked on each host:
 
         /etc/default/grub.dpkg-dist
         /etc/initramfs-tools/initramfs.conf.dpkg-dist

If other files come up, they should be added in the above decision
list, or in an operation in step 2 or 7 of the above procedure, before
the `clean_conflicts` call.

Files that should be updated in Puppet are mentioned in the Issues
section below as well.

# Service-specific upgrade procedures

WARNING: this section needs to be updated for bookworm.

## PostgreSQL upgrades

Note: *before* doing the entire major upgrade procedure, it is worth
considering upgrading PostgreSQL to "backports". There are no officiel
"Debian backports" of PostgreSQL, but there is an
<https://apt.postgresql.org/> repo which is *supposedly* compatible
with the official Debian packages. The only (currently known) problem
with that repo is that it doesn't use the tilde (`~`) version number,
so that when you do eventually do the major upgrade, you need to
manually upgrade those packages as well.

PostgreSQL is special and needs to be upgraded manually. 

 1. make a full backup of the old cluster:

        ssh -tt bungei.torproject.org 'sudo -u torbackup postgres-make-one-base-backup $(grep ^meronense.torproject.org $(which postgres-make-base-backups ))'

    The above assumes the host to backup is `meronense` and the backup
    server is `bungei`. See [howto/postgresql](howto/postgresql) for details of that
    procedure.

 3. Once the backup completes, on the database server, possibly stop
    users of the database, because it will have to be stopped for the
    major upgrade.
    
    on the Bacula director, in particular, this probably means waiting
    for all backups to complete and stopping the director:
    
        service bacula-director stop

    this will mean other things on other servers! failing to stop
    writes to the database *will* lead to problems with the backup
    monitoring system. an alternative is to just stop PostgreSQL
    altogether:
    
        service postgresql@11-main stop

    This also involves stopping Puppet so that it doesn't restart
    services:
    
        puppet agent --disable "PostgreSQL upgrade"

 2. On the storage server, move the directory out of the way and
    recreate it:

        ssh bungei.torproject.org "mv /srv/backups/pg/meronense /srv/backups/pg/meronense-11 && sudo -u torbackup mkdir /srv/backups/pg/meronense"

 3. on the database server, do the actual cluster upgrade:

        export LC_ALL=C.UTF-8 &&
        printf "about to drop cluster main on postgresql-13, press enter to continue" &&
        read _ &&
        pg_dropcluster --stop 13 main &&
        pg_upgradecluster -m upgrade -k 11 main &&
        for cluster in `ls /etc/postgresql/11/`; do
            mv /etc/postgresql/11/$cluster/conf.d/* /etc/postgresql/13/$cluster/conf.d/
        done

 4. change the cluster target in the backup system, in `tor-puppet`,
    for example:

        --- a/modules/postgres/manifests/backup_source.pp
        +++ b/modules/postgres/manifests/backup_source.pp
        @@ -30,7 +30,7 @@ class postgres::backup_source {
           # this block is to allow different cluster versions to be backed up,
           # or to turn off backups on some hosts
           case $::hostname {
        -    'materculae': {
        +    'materculae', 'bacula-director-01': {
               postgres::backup_cluster { $::hostname:
                 pg_version => '13',
               }

    ... and run Puppet on the server and the storage server (currently
    `bungei`).

 4. if services were stopped on step 3, restart them, e.g.:
 
        service bacula-director start
 
    or:
    
        service postgresql@13-main start

 5. change the postgres version in `tor-nagios` as well:

        --- a/config/nagios-master.cfg
        +++ b/config/nagios-master.cfg
        @@ -387,7 +387,7 @@ servers:
           materculae:
             address: 49.12.57.146
             parents: gnt-fsn
        -    hostgroups: computers, syslog-ng-hosts, apache2-hosts, apache-https-host, hassrvfs, postgres11-hosts
        +    hostgroups: computers, syslog-ng-hosts, apache2-hosts, apache-https-host, hassrvfs, postgres13-hosts
         
         
           # bacula storage

 6. make a new full backup of the new cluster:

        ssh -tt bungei.torproject.org 'sudo -u torbackup postgres-make-one-base-backup $(grep ^meronense.torproject.org $(which postgres-make-base-backups ))'

 7. make sure you check for gaps in the write-ahead log, see
    [tpo/tpa/team#40776](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40776) for an example of that problem and [the
    WAL-MISSING-AFTER PosgreSQL playbook](howto/postgresql#wal-missing-after) for recovery.

 8. once everything works okay, remove the old packages:

        apt purge postgresql-11 postgresql-client-11

 9. purge the old backups directory after a week:

        ssh bungei.torproject.org "echo 'rm -r /srv/backups/pg/meronense-11/' | at now + 7day"

It is also wise to read the [release notes](https://www.postgresql.org/docs/release/) for the relevant
release to see if there are any specific changes that are needed at
the application level, for service owners. In general, the above
procedure *does* use `pg_upgrade` so that's already covered.

## RT upgrades

TODO: verify to see if RT requires a kick.

## Ganeti upgrades

So far it seems there is no significant upgrade on the Ganeti
clusters, at least as far as Ganeti itself is concerned. There is a
bug with the newer Haskell code in bookworm but bookworm already has
[a patch](https://github.com/ganeti/ganeti/pull/1694) (really a workaround) designed to fix this.

## Puppet server upgrade

I had to `apt install postgresql puppetdb puppet-terminus-puppetdb`
and follow the [connect instructions](https://www.puppet.com/docs/puppetdb/7/connect_puppet_server.html), as I was using the redis
terminus before. I also had to `adduser puppetdb puppet` for it to be
able to access the certs, and add the certs to the
jetty config. Basically:

    certname="$(puppet config print certname)"
    hostcert="$(puppet config print hostcert)"
    hostkey="$(puppet config print hostprivkey)"
    cacert="$(puppet config print cacert)"

    adduser puppetdb puppet

    cat >>/etc/puppetdb/conf.d/jetty.ini <<-EOF
        ssl-host = 0.0.0.0
        ssl-port = 8081
        ssl-key = ${hostkey}
        ssl-cert = ${hostcert}
        ssl-ca-cert = ${cacert}
    EOF

    echo "Starting PuppetDB ..."
    systemctl start puppetdb

    cp /usr/share/doc/puppet-terminus-puppetdb/routes.yaml.example /etc/puppet/routes.yaml
    cat >/etc/puppet/puppetdb.conf <<-EOF
        [main]
        server_urls = https://${certname}:8081

also:

    apt install puppet-module-puppetlabs-cron-core puppet-module-puppetlabs-augeas-core puppet-module-puppetlabs-sshkeys-core
    puppetserver gem install trocla:0.4.0 --no-document

# Notable changes

Here is a list of notable changes from a system administration
perspective:

 * podman is getting close to usable TODO: update when/if 4.x hits
   bookworm, as this unleashes GitLab runner support and rootless
   containers ([bug 1007022](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1007022)), right now I need to use the
   experimental packages

See also the [wiki page about bookworm](https://wiki.debian.org/NewInBookworm) for another list.

## New packages

This is a curated list of packages that were introduced in
bookworm. There are actually *thousands* of new packages in the new
Debian release, but this is a small selection of projects I found
particularly interesting:

TODO

## Updated packages

This table summarizes package version changes I find interesting.

TODO

| Package     | Bullseye | Bookworm | Notes                       |
|-------------|----------|----------|-----------------------------|
| APT         |          |          |                             |
| Browserpass |          |          |                             |
| Docker      |          |          |                             |
| Emacs       |          |          |                             |
| Firefox     |          |          |                             |
| Ganeti      |          |          |                             |
| GNOME       |          |          |                             |
| Inkscape    |          |          |                             |
| Libreoffice |          |          |                             |
| OpenSSH     |          |          |                             |
| Postgresql  |          |          |                             |
| Python      | 3.9.2    | 3.11     | Python 2 removed completely |
| Puppet      |          |          |                             |

Note that this table may not be up to date with the current bullseye
release. See the [official release notes](https://www.debian.org/releases/bullseye/amd64/release-notes/ch-whats-new.en.html#newdistro) for a more up to date
list.

## Removed packages

TODO

Python 2 was completely removed from Debian, a long-term task that had
already started with bullseye, but not completed.

See also the [noteworthy obsolete packages](https://www.debian.org/releases/bookworm/amd64/release-notes/ch-information.en.html#noteworthy-obsolete-packages) list.

## Deprecation notices

TODO

# Issues

See also the official list of [known issues](https://www.debian.org/releases/bookworm/amd64/release-notes/ch-information.en.html).

## Pending

 * The official list of [known issues][]

[known issues]: https://www.debian.org/releases/buster/amd64/release-notes/ch-information.en.html

## Resolved

# Troubleshooting

## Upgrade failures

Instructions on errors during upgrades can be found in [the release
notes troubleshooting section](https://www.debian.org/releases/bookworm/amd64/release-notes/ch-upgrading.en.html#trouble).

## Reboot failures

If there's any trouble during reboots, you should use some recovery
system. The [release notes actually have good documentation on
that](https://www.debian.org/releases/bookworm/amd64/release-notes/ch-upgrading.en.html#recovery), on top of "use a live filesystem".

# References

 * [Official guide](https://www.debian.org/releases/bookworm/amd64/release-notes/ch-upgrading.en.html) (TODO: review)
 * [Release notes](https://www.debian.org/releases/bookworm/amd64/release-notes/ch-whats-new.en.html) (TODO: review)
 * [DSA guide](https://dsa.debian.org/howto/upgrade-to-bookworm/) (TODO: review)
 * [anarcat guide](https://anarc.at/services/upgrades/bookworm/) (WIP, last sync 2023-04-06)
 * [Solution proposal to automate this](https://wiki.debian.org/AutomatedUpgrade)

# Fleet-wide changes

The following changes need to be performed *once* for the entire
fleet, generally at the beginning of the upgrade process.

## installer changes

The installer need to be changed to support the new release. This
includes:

 * the Ganeti installers (add a `gnt-instance-debootstrap` variant,
   `modules/profile/manifests/ganeti.pp` in `tor-puppet.git`, see
   commit 4d38be42 for an example)
 * the (deprecated) libvirt installer
   (`modules/roles/files/virt/tor-install-VM`, in `tor-puppet.git`)
 * the wiki documentation:
   * create a new page like this one documenting the process, linked
     from [howto/upgrades](howto/upgrades)
   * make an entry in the `data.csv` to start tracking progress (see
     below), copy the `Makefile` as well, changing the suite name
   * change the [Ganeti procedure](howto/ganeti#adding-a-new-instance) so that the new suite is used by
     default
   * change the [Hetzner robot](howto/new-machine-hetzner-robot) install procedure
 * `tsa-misc` and the fabric installer (TODO)

## Debian archive changes

The Debian archive on `db.torproject.org` (currently alberti) need to
have a new suite added. This can be (partly) done by editing files
`/srv/db.torproject.org/ftp-archive/`. Specifically, the two following
files need to be changed:

 * `apt-ftparchive.config`: a new stanza for the suite, basically
   copy-pasting from a previous entry and changing the suite
 * `Makefile`: add the new suite to the for loop

But it is not enough: the directory structure need to be crafted by
hand as well. A simple way to do so is to replicate a previous release
structure:

    cd /srv/db.torproject.org/ftp-archive
    rsync -a --include='*/' --exclude='*' archive/dists/bullseye/  archive/dists/bookworm/

# Per host progress

Note that per-host upgrade policy is in [howto/upgrades](howto/upgrades).

When a critical mass of servers have been upgraded and only "hard"
ones remain, they can be turned into tickets and tracked in GitLab. In
the meantime...

A list of servers to upgrade can be obtained with:

    curl -s -G http://localhost:8080/pdb/query/v4 --data-urlencode 'query=nodes { facts { name = "lsbdistcodename" and value != "bullseye" }}' | jq .[].certname | sort

Or in Prometheus:

    count(node_os_info{version_id!="11"}) by (alias)

Or, by codename, including the codename in the output:

    count(node_os_info{version_codename!="bullseye"}) by (alias,version_codename)

<figure>
<img alt="graph showing planned completion date, currently around September 2020" src="/howto/upgrades/bullseye/predict.png" />
<figcaption>

The above graphic shows the progress of the migration between major
releases. It can be regenerated with the [predict-os](https://gitlab.com/anarcat/predict-os) script. It
pulls information from [puppet](howto/puppet) to update a [CSV file](data.csv) to keep
track of progress over time.

WARNING: the graph may be incorrect or missing as the upgrade
procedure ramps up. The following graph will be converted into a
Grafana dashboard to fix that, see [issue 40512](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40512).
</figcaption>
</figure>
