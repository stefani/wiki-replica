---
title: YubiKey documentation
---

YubiKeys are rugged security keys that can do FIDO2 two-factor
authentication (2FA), PKCS and OpenPGP operations, all inside a small
USB form factor.

[[_TOC_]]

# Tutorial

<!-- simple, brainless step-by-step instructions requiring little or -->
<!-- no technical background -->

# How to

## YubiKey training

This section holds the notes to the YubiKey training given in Costa
Rica in April 2023.

### Introduction

 * what is a YubiKey? it's a 2FA token with extra capabilities
 * why is it called a YubiKey? "Yubico's explanation of the name
   "YubiKey" is that it derives from the phrase "your ubiquitous key",
   and that "yubi" is the Japanese word for finger." ([Wikipedia](https://en.wikipedia.org/wiki/YubiKey#History),
   [source](https://www.yubico.com/about/about-us/))
 * what is 2FA? two-factor authentication
 * why do we need 2FA?
 
   * to make hacking your account more difficult,
   * ... and because people are not great at remembering good
     passwords
   * it's required by GitHub and our Nextcloud instance

 * why do we need a Yubikey? it's better than typical 2FA, it can
   protect against:
   * phishing attacks (and say why)
   * shoulder surfing and surveillance cameras
 * it cannot protect against:
   * malware on your computer (as they can control the YubiKey or
     steal your session cookie)
   * successful HTTPS MITM
 * what are we going to do today? 2FA only

"There's all sorts of pitfalls and challenges in deploying 2FA and
YubiKeys (e.g. "I lost my YubiKey" or "OMG GnuPG is hell"), we're not
going to immediately solve all of those issues. We're going to get
hardware into people's hands and hopefully train them with U2F/FIDO2
web 2FA, and maybe be able to explore the SSH/OpenPGP side of things
as well."

### Unpacking and authenticating a YubiKey

 * check blister packaging
 * login to <https://www.yubico.com/genuine/>

### Setting up 2FA in Nextcloud

We can either follow the [upstream guide](https://docs.nextcloud.com/server/latest/user_manual/en/user_2fa.html) or [our own
tutorial](#signing-in-and-setting-up-two-factor-authentication). Here's a copy of the latter with only the U2F
instructions:

 1. In NextCloud, select Settings -> Security. The link to your
    settings can be found by clicking on your "user icon" in the top
    right corner. Direct link: [Settings -> Security](https://nc.torproject.net/settings/user/security).
 2. Pick either the [U2F device](https://en.wikipedia.org/wiki/Universal_2nd_Factor) as an "second factor".
 3. Click the "Add U2F device" button under the "U2F device" section
 4. Insert the token and press the button when prompted by your web
    browser
 5. Enter a name for the device and click "Add"
 6. Click "Generate Backup codes" in the Two-Factor Authentication
    section of that page.
 7. Save your backup codes to a password manager of your choice. These
    will be needed to regain access to your NextCloud account if you
    ever lose your 2FA token/application.
 8. Log out and log in again, to verify that you got two factor
    authentication working.

### Setting up 2FA in GitLab

TODO

## SSH authentication in FIDO2 mode

Recent YubiKeys like the YubiKey 5 ship a "FIDO2" applet that is
generally used for two-factor authentication. But SSH also supports
using that to store SSH keys, which can therefore be used to
authenticate against servers.

[This Yubico guide](https://developers.yubico.com/SSH/Securing_SSH_with_FIDO2.html) shows you how to configure such keys,
recognizable from their `-sk` suffix (e.g. `ed25519-sk`). See also
[this GitHub guide](https://github.blog/2021-05-10-security-keys-supported-ssh-git-operations/).

This is the recommended method for users who want to use their YubiKeys for SSH
connections to GitLab, GitHub, Debian servers, etc.

It should be noted that the `-sk` SSH keys are relatively new, and as such are
often not supported by old devices and servers. Users who would like to to use
their YubiKey to secure connections to such older SSH servers may use one of
the modes below, in addition to native FIDO2 keys.

## SSH authentication in OpenPGP mode

The YubiKeys also ship with an "OpenPGP smartcard applet" that allows you
to store cryptographic keys. The YubikKey 5 in particular supports ECC
keys.

[This guide](https://github.com/drduh/YubiKey-Guide) will allow you to use OpenPGP to store keys on the
YubiKey and then use that key to authenticate to SSH servers. TPA may
eventually sublime this rather long guide in a simpler version
specifically tailored for you, possibly based on [anarcat's guide](https://anarc.at/blog/2015-12-14-yubikey-howto/#configuring-a-pin).

Also review the [Ultimate Yubikey Setup Guide with ed25519!](https://zach.codes/ultimate-yubikey-setup-guide/) and
the [other documentation](#other-documentation) section.

## SSH RSA authentication in PIV mode

This guide should be followed if you want to use SSH without depending
on OpenPGP *and* FIDO2.

### Token setup

YubiKey 5-series tokens, which support the [FIPS 201](https://en.wikipedia.org/wiki/FIPS_201)
standard also known as PIV, can be used as a convenient second factor to for ssh
public key authentication.

While the YubiKey supports either RSA or ECC certificates for this, we'll use
RSA since it's the most compatible across all SSH servers. For example, some BMC
only support `ssh-rsa` keys. This has also been observed on Pantheon.io, a DevOps
platform for websites. For modern SSH servers, the `ed25519-sk` key type is
preferred.

First, one must install [yubikey-manager](https://github.com/Yubico/yubikey-manager).
On Debian 11 (bullseye), a simple `apt install yubikey-manager` is sufficient. On
older versions of Debian, one should install it via `pip3` in order to have a
sufficiently recent version of the tool.

 1. Reset all PIV config/data on the token: `ykman piv reset`
 2. Define new PIN (the default is `123456`): `ykman piv change-pin`
   * *The PIN must be between 6 and 8 characters long, and supports any type of
   alphanumeric characters. For cross-platform compatibility, numeric digits are
   recommended.*
 3. Define new PUK (Personal Unblocking Key, used when PIN retries have been
 exceeded): `ykman piv change-puk`
 2. Define a management key: `piv change-management-key -pt`
 3. Generate RSA key: `ykman piv keys generate --algorithm RSA2048 --pin-policy
 ONCE --touch-policy CACHED 9a pubkey.pem`
 4. Generate certificate: `ykman piv certificates generate --valid-days 3650
 --subject "CN=ssh" 9a pubkey.pem`
 5. Verify with `ykman piv info`

This will create a 2048-bits RSA certificate in slot 9a of the PIV token device.
The PIN will be required only once per-session (`--pin-policy ONCE`) but touch
will be required at every use and remembered for 15 seconds afterwards
(`--touch-policy CACHED`).

The next step is to install and start [yubikey-agent](https://github.com/FiloSottile/yubikey-agent)
which is a small daemon written in Go that act as an ssh-agent for the YubiKey.
Installation instructions which work with Debian can be found here:

https://github.com/FiloSottile/yubikey-agent/blob/main/systemd.md

The `yubikey-agent -setup` step can be skipped, as we've already set up the
token with the above. `yubikey-agent`'s own setup routine makes different,
hard-coded choices with regard to the PIN and PUK (identical), certificate type
(ECC) and other small things.

Once the agent is setup and running in the background, the SSH public key can be
retrieved from the token using the following command: `ssh-add -L`.

At this point it may be useful to install the `libnotify-bin` package on Debian
which allows the agent to send a desktop notification when the token needs to be
touched to perform an authentication operation. This is especially useful when
the token LED, which flashes when touch is requested, isn't well into view.

These instructions are spinned off from those found at: https://eta.st/2021/03/06/yubikey-5-piv.html

### Configure SSH

If not done already, now is a good time to setup the ssh configuration for the
TPO jump host, see [ssh-jump-host](/doc/ssh-jump-host/) for these instructions.

To have the `ssh` command use `yubikey-agent` when connecting to TPO hosts, add
this line in `~/.ssh/config` under `Host *.torproject.org`:

    IdentityAgent /run/user/1000/yubikey-agent/yubikey-agent.sock

If you also want to use `ed25519_sk`-type keys based on the modern FIDO2
protocol for non-TPO hosts, you may add this at the end of `~./ssh/config`:

    Host *
      IdentityAgent /dev/null
      IdentityFile ~/.ssh/id_ed25519_sk

## OpenPGP operations

YubiKeys can also be used for general operation with OpenPGP,
regardless of purpose. For signatures, the operation is relatively
similar to the [SSH guide above](#ssh-authentication-in-openpgp-mode), except there's no need to do any
SSH-specific configuration.

### Special considerations for storing encryption keys

For *encryption* keys, however, special care need to be taken as the
loss of a YubiKey could be catastrophic: in such a case, while the key
can be revoked, that doesn't allow the operator to recover past
material encrypted with the key.

Encryption keys, therefore, must *not* be generated on the YubiKey as
they *MUST* be backed up. They therefore *MUST* be generated on
another device.

The general strategy here is to have three copies of the encryption
key:

 1. `main key`: a first YubiKey used for daily operation
 2. `backup key`: a *second* YubiKey available as a backup
 3. `backup disk`: a copy of the encryption key material stored on a
    normal disk, encrypted with itself

The rationale here is that if the `main key` is lost, the `backup key`
and `backup disk` can be *combined* to create a *new* `main key`.

If the `backup disk` did not exist, it would be impossible to recreate
a new `main key` and, when the `backup key` is eventually lost or
destroyed, the encrypted contents will not be readable anymore.

## FAQ

### I don't have usb-c in my laptop, would i need an adaptor then?

If you get a USB-A key, yes, but you can get a USB-C key!

### Who should use this?

Everyone! If you're using a service like Nextcloud, the Discourse
forum, GitLab, you should enable 2FA and preferably with a
cryptographic token. That's not yet official policy, but it's probably
going to hit the security policy in some shape or form in the future.

### I do my work from Tails, do I need a Yubikey?

Yes, because Tails doesn't necessarily protect you against phishing attacks.

### Can I use the USB port during my work session, or i need to have the YubiKey plugged all the time?

You don't need to have it plugged in all the time.

One interesting aspect of the YubiKey is that you can unplug it and
decide "nope, authentication doesn't happen here anymore".

It's a clear way to secure that cryptographic material, physically.

### Any reason why we pick a Yubikey and not a tool with a open design like a NitroKey?

anarcat made a [review of the Nitrokey in 2017](https://anarc.at/blog/2017-10-26-comparison-cryptographic-keycards/) and found that
their form factor was less reliable than the YubiKey.

The solokey was also considered but is not quite ready for prime time
yet. Google's Titan key was also an option but only supports 2FA (not
OpenPGP or SSH), see the [other alternatives](#other-alternatives) section for more
details.

### My Yubikey squirts out an OTP code when I accidentally touch it

There are several ways to deal with this issue. Since we don't use Yubico OTP in
Tor, the easiest solution is to simply disable the OTP app on the USB interface.

First, ensure the Yubikey is inserted in one of your USB ports.

On the command-line, you can install the `yubikey-manager` package and run the
command below:

    ykman config usb --disable otp

This program is also available with a GUI, installed with `yubikey-manager-qt`
on Debian-based systems. Installers for other platforms such as Windows and
MacOS can be downloaded from the [Yubico website download page](https://www.yubico.com/support/download/yubikey-manager/).

The procedure with the Yubikey Manager GUI is to open the program, click the
`Interfaces` tab, and under `USB`, uncheck `OTP` and click `Save interfaces`.

Once this is done, OTP will remain disabled until it's manually re-enabled.

If you want to conserve the ability to generate Yubico OTP codes, there are two
options: either disable sending the `<Enter>` character using `ykman otp
settings --no-enter 1`, or swap the OTP to slot 2, which requires a sustained
2-second touch to activate, with `ykman otp swap`.

## Pager playbook

<!-- information about common errors from the monitoring system and -->
<!-- how to deal with them. this should be easy to follow: think of -->
<!-- your future self, in a stressful situation, tired and hungry. -->

## Disaster recovery

<!-- what to do if all goes to hell. e.g. restore from backups? -->
<!-- rebuild from scratch? not necessarily those procedures (e.g. see -->
<!-- "Installation" below but some pointers. -->

# Reference

## Installation

When you receive your YubiKey, you need to first inspect the "blister"
package to see if it has been tampered with.

Then, open the package, connect the key to a computer and visit this
page in a web browser:

<https://www.yubico.com/genuine/>

This will guide you through verifying the key's integrity.

Out of the box, the key should work for two-factor authentication with
FIDO2 on most websites. It is imperative that you keep a copy of the
backup or "scratch" codes that are usually provided when you setup 2FA
on the site, as you may lose the key and that is the only way to
recover from that.

For other setups, see the following how-to guides:

 * [SSH RSA authentication in PIV mode](#ssh-rsa-authentication-in-piv-mode)

## Upgrades

YubiKeys cannot be upgraded, the firmware is read-only.

## SLA

N/A

## Design and architecture

A YubiKey is an integrated circuit that performs cryptographic
operations on behalf of a host. In a sense, it is a tiny air-gapped
computer that you connect to a host, typically over USB but Yubikeys
can also operate over NFC.

## Services

N/A

## Storage

The YubiKeys keep private cryptographic information embedded in the
key, for example RSA keys for the SSH authentication mechanism. Those
keys are supposed to be impossible to extract from the YubiKey, which
means they are also impossible to backup.

## Queues

N/A

## Interfaces

YubiKeys use a few standards for communication:

 * FIDO2 for 2FA
 * PIV for SSH authentication
 * OpenPGP "smart card" applet for OpenPGP signatures, authentication
   and encryption

## Authentication

It's possible to verify the integrity of a key by visiting:

<https://www.yubico.com/genuine/>

## Implementation

The firmware on YubiKeys is proprietary and closed source, a major
downside to this platform.

## Related services

YubiKeys can be used to authenticate with the following services:

| Service       | Authentication type |
|---------------|---------------------|
| [Discourse][] | 2FA                 |
| [GitLab][]    | 2FA, SSH            |
| [Nextcloud][] | 2FA                 |

[Discourse]: service/forum
[GitLab]: howto/gitlab
[Nextcloud]: service/nextcloud

## Issues

There is no issue tracker specifically for this project, [File][] or
[search][] for issues in the [team issue tracker][search] with the
label ~Foo.

 [File]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/new
 [search]: https://gitlab.torproject.org/tpo/tpa/team/-/issues?label_name%5B%5D=Foo

## Maintainer

anarcat worked on getting a bunch of YubiKeys shipped to a Tor meeting
in 2023, and is generally the go-to person for this, with a fallback
on TPA.

## Users

All tor-internal people are expected to have access to a YubiKey and
know how to use it.

## Upstream

YubiKeys are manufactured by [Yubico](https://www.yubico.com), a company headquartered in
Palo Alto in California, but with Swedish origins. It has [merged with
a holding company from Stockholm](https://www.yubico.com/blog/yubico-is-merging-with-acq-bure/) in April 2023.

## Monitoring and metrics

N/A

## Tests

N/A

## Logs

N/A

## Backups

YubiKeys backups are complicated by the fact that you can't actually
extract the secret key from a YubiKey.

### FIDO2 keys

For 2FA, there's no way around it: the secret is generated on the key
and stays on the key. The mitigation is to keep a copy of the backup
codes in your password manager.

### OpenPGP keys

For OpenPGP, you may want to generate the key outside the YubiKey and
copy it in, that way you can backup the private key somewhere. A
robust and secure backup system for this would be made in three parts:

 1. the main YubiKey, which you use every day
 2. a backup YubiKey, which you can switch to if you lose the first
    one
 3. a copy of the OpenPGP secret key material, encrypted with itself,
    so you can create a second key when you lose a key

The idea of the last backup is that you can recover the key material
from the first key with the second key and make a new key that
way. It may seem strange to encrypt a key with itself, but it is
actually relevant in this specific use case, because another copy of
the secret key material is available on the backup YubiKey.

## Other documentation

 * [Anarcat's old (2015) YubiKey howto](https://anarc.at/blog/2015-12-14-yubikey-howto/)
 * [A YubiKey cheatsheet](https://debugging.works/blog/yubikey-cheatsheet/)
 * [TPA-RFC-53][] and [discussion ticket](https://gitlab.torproject.org/tpo/tpa/team/-/issues/41083)

[TPA-RFC-53]: policy/tpa-rfc-53-security-keys

# Discussion

While we still have to make an all-encompassing security policy
([TPA-RFC-18](policy/tpa-rfc-18-security-policy)), we have decided in April 2023 to train our folks to
use YubiKeys as security keys, see [TPA-RFC-53][] and [discussion
ticket](https://gitlab.torproject.org/tpo/tpa/team/-/issues/41083). This was done following a survey posted to tor-internal,
the results of which are available in [this GitLab comment](https://gitlab.torproject.org/tpo/tpa/team/-/issues/41083#note_2887000).

## Requirements

The requirements checklist was:

 * FIDO2/U2F/whatever this is called now
 * physical confirmation button (ideally "touch")
 * OpenPGP applet should be available as an option
 * USB A or USB-C?
 * RSA, and ed5519 or equivalent?

It should cover the following use cases:

 * SSH (through the SK stuff or gpg-agent + openpgp auth keys)
 * OpenPGP
 * web browsers (e.g. gitlab, discourse, nextcloud, etc)

## Security and risk assessment

### Background

TPA (Tor Project system Administrators) is looking at strengthening
our security by making sure we have stronger two-factor authentication
(2FA) everywhere. We have mandatory 2FA on some services, but this can
often take the form of phone-based 2FA which is prone to social
engineering attacks.

This is important because some high profile organizations like ours
were compromised by hacking into key people's accounts and destroying
critical data or introducing vulnerabilities in their software. Those
organisations had 2FA enabled, but attackers were able to bypass that
security by hijacking their phones, which is why having a
cryptographic token like a YubiKey is important.

We also don't necessarily provide people with the means to more
securely store their (e.g. SSH) private keys, used commonly by
developers to push and sign code. So we are considering buying a bunch
of YubiKeys, bringing them to the next Tor meeting, and training
people to use them.

There's all sorts of pitfalls and challenges in deploying 2FA and
YubiKeys (e.g. "i lost my YubiKey" or "omg GnuPG is hell"). We're not
going to immediately solve all of those issues. We're going to get
hardware into people's hands and hopefully train them with U2F/FIDO2
web 2FA, and maybe be able to explore the SSH/OpenPGP side of things
as well.

### Threat model

The main threat model is phishing, but there's another threat actor to
take into account: powerful state-level adversaries. Those have the
power to intercept and manipulate packages as they ship for
example. For that reason, we were careful in how the devices were
shipped, and they were handed out in person at an in-person
meeting.

Users are also encouraged to authenticate their YubiKey using the
Yubico website, which should provide a reliable attestation that the
key was really made by Yubico.

That assumes trust in the corporation, of course. The rationale there
is the reputation cost for YubiKey would be too high if they allowed
backdoors in their services, but it is of course a possibility that a
rogue employee (or Yubico itself) could leverage those devices to
successfully attack the Tor project.

### Future work

Ideally, there would be a rugged *and* open-hardware device that could
simultaneously offer the tamper-resistance of the YubiKey while at the
same time providing an auditable hardware platform.

## Technical debt and next steps

At this point, we need to train users on how to use those devices, and
factor this in a broader security policy ([TPA-RFC-18](policy/tpa-rfc-18-security-policy)).

## Proposed Solution

This was adopted in [TPA-RFC-53][], see also the [discussion
ticket](https://gitlab.torproject.org/tpo/tpa/team/-/issues/41083).

## Other alternatives

 * [tillitis.se](https://tillitis.se/): not ready for end-user adoption yet
 * [Passkeys](https://fidoalliance.org/passkeys/) are promising, but have their own pitfalls. They
   certainly do not provide "2FA" in the sense that they do not add an
   extra authentication mechanism on top of your already existing
   passwords. Maybe that's okay? It's still early to tell how well
   passkeys will be adopted and whether they will displace traditional
   mechanisms or not.
 * [Nitrokey](https://www.nitrokey.com/fr): not rugged enough
 * [Solokey](https://solokeys.com/): still at the crowdfunding stage
 * [FST-01](https://www.gniibe.org/FST-01/fst-01.html): EOL, hard to find, gniibe is working on a [smartcard
   reader](https://www.gniibe.org/memo/development/ttxs/ttxs-hardware-another.html)
 * [Titan keys](https://cloud.google.com/titan-security-key/): FIDO2 only, but ships built-in with Pixel phones
