# Billing and ordering

This is kind of hell.

You need to register on their site and pay with a credit card. But you
can't be from the US to order in Canada and vice-versa, which makes
things pretty complicated if you want to have stuff in one country
*or* the other.

Also, the US side of things can trip over itself and flag you as a
compromised account, at which point they will ask you for a driver's
license and so on. A workaround is to go on the other site.

Once you have ordered the server, they will send you a confirmation
email, then another email when the order is fulfilled, with the
username and password to login to the server. Next step is to setup
the server.

# Preparation

We assume we are creating a new server named
`test-01.torproject.org`. You should have, at this point, received a
email with the username and password. Ideally, you'd login through the
web interface's console, which they call the "KVM".

 1. immediately change the password so the cleartext password sent by
    email cannot be reused, document in the password manager

 2. change the hostname on the server *and* in the web interface to
    avoid confusion:
    
        hostname test-01
        exec bash

    In the OVH dashboard, you need to:
    
      1. navigate to the "product and services" (or "bare metal
         cloud" then "Virtual private servers")
      2. click on the server name
      3. click on the "..." menu next to the server name
      4. choose "change name"

 3. setting up reverse DNS doesn't currently work ("An error has
    occurred updating the reverse path."), pretend this is not a
    problem

 4. add your SSH key to the root account

 4. then follow the normal [new-machine procedure](howto/new-machine),
    with the understanding reverse DNS is broken and that we do not
    have full disk encryption

In particular, you will have to:

 1. reset the `/etc/hosts` file (with fabric works)

 2. hack at `/etc/resolv.conf` to change the `search` domain

 3. delete the `debian` account

See [issue tpo/tpa/team#40904](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40904) for an example run.
