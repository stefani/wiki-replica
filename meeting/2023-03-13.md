# Roll call: who's there and emergencies

anarcat, gaba, kez, lavamind

# Q1 prioritisation

Discuss the priorities for the remaining month, consider Q2.

Donate page, Ganeti "dal" cluster and the Discourse self-hosting are
the priorities.

Completing the bullseye upgrades and converting the installers to
bookworm would be nice, alongside pushing some proposals ahead (email,
gitolite, etc).

# Dashboard review

We reviewed the dashboards like in our usual per-user check-in:
 
 * https://gitlab.torproject.org/groups/tpo/-/boards?scope=all&utf8=%E2%9C%93&assignee_username=anarcat
 * https://gitlab.torproject.org/groups/tpo/-/boards?scope=all&utf8=%E2%9C%93&assignee_username=kez
 * https://gitlab.torproject.org/groups/tpo/-/boards?scope=all&utf8=%E2%9C%93&assignee_username=lavamind

General dashboards:

 * https://gitlab.torproject.org/tpo/tpa/team/-/boards/117
 * https://gitlab.torproject.org/groups/tpo/web/-/boards
 * https://gitlab.torproject.org/groups/tpo/tpa/-/boards

# Early vacation planning

We went over people's planned holidays and things look okay, not too
much overlap. Don't forget to ask for your holidays in advance as per
the handbook.

# Metrics of the month

 * hosts in Puppet: 97, LDAP: 98, Prometheus exporters: 167
 * number of Apache servers monitored: 32, hits per second: 658
 * number of self-hosted nameservers: 6, mail servers: 9
 * pending upgrades: 0, reboots: 0
 * average load: 0.58, memory available: 5.92 TiB/7.04 TiB, running
   processes: 783
 * disk free/total: 34.43 TiB/92.96 TiB
 * bytes sent: 354.56 MB/s, received: 211.38 MB/s
 * planned bullseye upgrades completion date: 2022-12-29 (!)
 * [GitLab tickets][]: 177 tickets including...
   * open: 1
   * icebox: 141
   * backlog: 22
   * next: 4
   * doing: 7
   * needs information: 2
   * (closed: 3070)
    
 [Gitlab tickets]: https://gitlab.torproject.org/tpo/tpa/team/-/boards

Upgrade prediction graph lives at:

https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/upgrades/bullseye/

Obviously, the planned date is incorrect. We are lagging behind on the
hard core of ~10 machines that are trickier to upgrade.
